(function(exports) { 
	var BrowserModel = Backbone.Model.extend({
		defaults: {
			width: 300,
			height: 300
		}
	});
	exports.BrowserModel = BrowserModel;
})(window);
(function(exports) { 
	var IconItemModel = Backbone.Model.extend({
		idAttribute: "id",
		defaults: {
			text: "",
			image: "",
			checked: false
		}
	});
	exports.IconItemModel = IconItemModel;
})(window);
(function(exports) { 
	var Itemlist = Backbone.Collection.extend({
		model: IconItemModel
	});
	exports.Itemlist = Itemlist;
})(window);
(function(exports) { 
	var BrowserIconView = Backbone.View.extend({
		itemListClass: 'itemList',
		renderIds: [],
		initItemRenderCount: 2,
		initialize: function(options) {
			_.extend(this, Backbone.Events);
			this.itemList = options.itemlist;

			this.render();
			
			_.bindAll(this, 'scrollView');
			$(".iconView").scroll(this.scrollView);
			
			this.scrollView();
		},
		events: {
			 "click .currentItem" : "clickedOnItem"
			,"click #toggleItems" : "toggleAllItems"
			,"click #removeItem" : "removeItem"
			,"click #addItem"	: "addItem"
			,"onEndResize .iconView" : "scrollView"
		},

		removeItem: function() {
			that = this;

			var models = _.filter(this.itemList.models,function (item){
				return item.get('checked') == true && _.contains(that.renderIds,item.id);
			});
			
			_.each(models,function(model) {
				that.itemList.remove(model);

				// remove from renderIds 
				index = that.renderIds.indexOf(model.id);
				that.renderIds.splice(model.id, 1);
			});

			
		},
		scrollView: function() {
			that = this;
			var scrolltop=	$(".iconView").scrollTop();
			var scrollheight=	$('.itemList').height();
			var windowheight=	$('.iconView ').height();
			var scrolloffset = 20;
			if(scrolltop>=(scrollheight-(windowheight+scrolloffset)))
			{
			    //подгружаем новые элементы
			   that.loadNewItems();
			   if(that.haveAnyMoreItems()) {
			   that.scrollView();
			   }
			}
			
		},
		haveAnyMoreItems: function() {
			countOfItem = this.itemList.models.length;
			counter =  0; 
			_.each(this.itemList.models,function(item) {
				if(_.contains(that.renderIds,item.id)) {
					counter++;
				}
			});
			//console.log(counter,countOfItem);
			return counter < countOfItem;
		},
		addItem: function() {
			type = $( ".typeOfElement option:selected" ).val();

			var item = new IconItemModel({
				id: this.getNextId(),
				text: type,
				image: "images/" + type + ".png",
				checked: false
			});
			this.renderIds.push(item.id);
			this.itemList.add(item);
			this.renderItem(item);
		},
		getNextId: function() {
			var tmpId = 1;
			var ids = [];
			_.each(this.itemList.models,function(item){
				ids.push(item.id);
			});
			while(_.contains(ids,tmpId)){
				tmpId++;
			}
			return tmpId;
		},
		clickedOnItem: function(el) {
			var id = $(el.currentTarget).attr('data-id');

			if(this.isSelected(id)) {
				this.toggleCheckItems(id,false);
			} else {
				this.toggleCheckItems(id,true);
			}
		},

		toggleAllItems: function() {
			console.log('checked');
			var models = _.find(this.itemList.models,function (item){
				return item.get('checked') == true && _.contains(that.renderIds,item.id);
			});
			//console.log(models);
			that = this;
			if(models != undefined) {
				_.each(this.itemList.models,function(item) {
					if(_.contains(that.renderIds,item.id)) {
						item.set('checked',false);
					}
				});
			} else {
				_.each(this.itemList.models,function(item){
					if(_.contains(that.renderIds,item.id)) {
						item.set('checked',true);
					}
				});
			}
		},
		toggleCheckItems: function(ids,toggle) {
			var models = [];
			that = this;
			_.each([].concat(ids), function(id) {
				state = _.find(that.itemList.models, function (item) {
					return item.id == id && _.contains(that.renderIds,item.id);

				});
				
				if(state != undefined) {
					state.set("checked",toggle);
				}
				
			});
			
		},

		isSelected: function(id) {

			state = _.find(this.itemList.models, function (item) {
				return item.id == id;
			});
			
			if(state != undefined) {
				return state.attributes.checked;
			} else {
				return null
			}
		},
		loadNewItems: function() {
			that = this;
			var counter = 1;
			//console.log(this.itemList.models.length);
			//console.log(that.renderIds.length);
			_.each(this.itemList.models, function (item) {
                if(that.initItemRenderCount >= counter && !_.contains(that.renderIds,item.id)){
 					that.renderItem(item);
                	that.renderIds.push(item.id);
                	counter++;
                }

            });
            return this;
		},
		render: function() {
			// set init width and height
			this.$el.find('.iconView').width(this.model.attributes.width).height(this.model.attributes.height);
			that = this;
			// render items
			var counter = 1;
			_.each(this.itemList.models, function (item) {
                if(that.initItemRenderCount < counter){
 					return;
                }
                that.renderItem(item);
                that.renderIds.push(item.id);
                counter++;

            });

			return this;
		},
		renderItem: function(item) {

			var itemView = new ItemView({
			    model: item
			});

			this.$el.find('.' + this.itemListClass).append(itemView.render().el);
		}
	});
	exports.BrowserIconView = BrowserIconView;

})(window);
(function(exports) { 
	var ItemView = Backbone.View.extend({
		width: 35,
		height: 25,
		tagName: 'div',
   		className: 'currentItem',
		templateId: 'IconItem-tmpl',
		initialize: function(options) {

			if(options.width) {
				this.width = options.width;
			}
			if(options.height) {
				this.height = options.height;
			}

			this.render();
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		removeView: function() {
			this.stopListening();
			this.$el.remove();
		},

		render: function () {

			var tmpl = _.template($("#" + this.templateId).html());
			this.$el.html(tmpl(this.model.toJSON()));
			
			this.$el.attr('data-id', this.model.get('id'));

			// checked class
			if(this.model.get('checked') == true) {
				this.$el.addClass("checked");
			} else {
				this.$el.removeClass("checked");
			}
			return this;
		}
	});
	exports.ItemView = ItemView;
})(window);
$(document).ready(function() {
	// items
	var demoItems = [
		 {id:1, image:"images/photo.png", text:"photo", checked: false}
		,{id:2, image:"images/video.png", text:"video", checked: false}
		,{id:3, image:"images/text.png", text:"text", checked: true}
		,{id:4, image:"images/music.png", text:"music", checked: true}
		,{id:5, image:"images/photo.png", text:"photo", checked: false}
		,{id:6, image:"images/video.png", text:"video", checked: true}
		,{id:7, image:"images/music.png", text:"music", checked: true}
		,{id:8, image:"images/video.png", text:"video", checked: false}
		,{id:9, image:"images/photo.png", text:"photo", checked: false}
		,{id:10, image:"images/text.png", text:"text", checked: true}
		,{id:11, image:"images/video.png", text:"video", checked: true}
		,{id:12, image:"images/text.png", text:"text", checked: false}
		,{id:13, image:"images/video.png", text:"video", checked: false}
		,{id:14, image:"images/photo.png", text:"photo", checked: true}
		,{id:15, image:"images/music.png", text:"music", checked: true}
		,{id:16, image:"images/photo.png", text:"photo", checked: false}
		,{id:17, image:"images/text.png", text:"text", checked: true}
		,{id:18, image:"images/video.png", text:"video", checked: true}
		,{id:19, image:"images/video.png", text:"video", checked: false}
		,{id:20, image:"images/text.png", text:"text", checked: false}
		,{id:21, image:"images/video.png", text:"video", checked: false}
		,{id:22, image:"images/photo.png", text:"photo", checked: true}
		,{id:23, image:"images/music.png", text:"music", checked: true}
		,{id:24, image:"images/photo.png", text:"photo", checked: false}
		,{id:30, image:"images/text.png", text:"text", checked: true}
		,{id:31, image:"images/video.png", text:"video", checked: true}
		,{id:32, image:"images/text.png", text:"text", checked: false}
		,{id:33, image:"images/video.png", text:"video", checked: false}
		,{id:34, image:"images/photo.png", text:"photo", checked: true}
		,{id:35, image:"images/music.png", text:"music", checked: true}
		,{id:36, image:"images/photo.png", text:"photo", checked: false}
		,{id:37, image:"images/text.png", text:"text", checked: true}
		,{id:38, image:"images/video.png", text:"video", checked: true}
		,{id:40, image:"images/text.png", text:"text", checked: false}
	];

	
	window.itemlist = new Itemlist(demoItems);
	
	// console.log(itemlist);
	
	window.browserModel = new BrowserModel({
		width: 400, 
		height: 400
	});

	window.browserIconView = new BrowserIconView({
		el: $(".browserWrapper"),
		model: browserModel,
		itemlist: itemlist
	});

	$(".resizable")
      .wrap('<div/>')
        .css({'overflow':'hidden'})
          .parent()
            .css({'display':'inline-block',
                  'overflow':'hidden',
                  'height':function(){return $('.resizable',this).height();},
                  'width':  function(){return $('.resizable',this).width();},
                  // 'paddingBottom':'12px',
                  // 'paddingRight':'12px'
                  'padding':'10px'
                }).resizable()
                    .find('.resizable')
                      .css({overflow:'auto',
                            width:'100%',
                            height:'100%'});


});

