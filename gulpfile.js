var gulp = require('gulp'),
    less = require('gulp-less'),
    livereload = require('gulp-livereload'),
    myth = require('gulp-myth'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect');

// Move html
gulp.task('html', function() {
  gulp.src('./dev/*.html')
    .pipe(gulp.dest('./public/'))
    .pipe(livereload());
});

// Compile less, then move
gulp.task('less', function() {
  gulp.src('./dev/css/*.less')
    .pipe(less())
    .pipe(myth())
    .pipe(gulp.dest('./public/css/'))
    .pipe(livereload());
});

gulp.task('vendor-js', function() {
  gulp.src(['./dev/js/vendor/**/*.js'])
    .pipe(gulp.dest('./public/js'));
});

// Concat js, then move
gulp.task('js', function() {
  var path = './dev/js/';
  var cols = path + '/modules/collections/*.js';
  var mods = path + '/modules/models/*.js';
  var views = path + '/modules/views/*.js';
  var sources = [
    path + 'modules/*.js',
    mods,
    cols,
    views,
    path + 'init.js',
    '!'+ path +'vendor/**/*.js'
  ];
  gulp.src(sources)
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./public/js'))
    .pipe(livereload());
});

// Start web-server
gulp.task('connect', function() {
  connect.server({
    port: 8000,
    root: 'public',
    livereload: true
  });
});

gulp.task('watch', function() {
  gulp.run('html');
  gulp.run('less');
  gulp.run('vendor-js');
  gulp.run('js');

  livereload.listen();
  gulp.watch('dev/*.html', ['html']);
  gulp.watch('dev/js/**/**/*', ['js']);
  gulp.watch('dev/css/*.less', ['less']);

  gulp.run('connect');
});

gulp.task('build', function() {
    gulp.src('./dev/css/*.less')
      .pipe(less())
      .pipe(myth())
      .pipe(gulp.dest('./build/css/'))

    gulp.src(['./dev/js/**/*.js', '!./dev/js/vendor/**/*.js'])
      .pipe(concat('index.js'))
      .pipe(gulp.dest('./build/js'));

    gulp.src(['./dev/js/**/*.js'])
      .pipe(gulp.dest('./build/**/*.js'));

    gulp.src(['./dev/*.html'])
      .pipe(gulp.dest('./build/*.js'));
});