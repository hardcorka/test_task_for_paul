(function(exports) { 
	var ItemView = Backbone.View.extend({
		width: 35,
		height: 25,
		tagName: 'div',
   		className: 'currentItem',
		templateId: 'IconItem-tmpl',
		initialize: function(options) {

			if(options.width) {
				this.width = options.width;
			}
			if(options.height) {
				this.height = options.height;
			}

			this.render();
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		removeView: function() {
			this.stopListening();
			this.$el.remove();
		},

		render: function () {

			var tmpl = _.template($("#" + this.templateId).html());
			this.$el.html(tmpl(this.model.toJSON()));
			
			this.$el.attr('data-id', this.model.get('id'));

			// checked class
			if(this.model.get('checked') == true) {
				this.$el.addClass("checked");
			} else {
				this.$el.removeClass("checked");
			}
			return this;
		}
	});
	exports.ItemView = ItemView;
})(window);