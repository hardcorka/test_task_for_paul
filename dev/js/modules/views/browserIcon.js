(function(exports) { 
	var BrowserIconView = Backbone.View.extend({
		itemListClass: 'itemList',
		renderIds: [],
		initItemRenderCount: 2,
		initialize: function(options) {
			_.extend(this, Backbone.Events);
			this.itemList = options.itemlist;

			this.render();
			
			_.bindAll(this, 'scrollView');
			$(".iconView").scroll(this.scrollView);
			
			this.scrollView();
		},
		events: {
			 "click .currentItem" : "clickedOnItem"
			,"click #toggleItems" : "toggleAllItems"
			,"click #removeItem" : "removeItem"
			,"click #addItem"	: "addItem"
			,"onEndResize .iconView" : "scrollView"
		},

		removeItem: function() {
			var that = this;

			var models = _.filter(this.itemList.models,function (item){
				return item.get('checked') == true && _.contains(that.renderIds,item.id);
			});
			
			_.each(models,function(model) {
				that.itemList.remove(model);

				// remove from renderIds 
				index = that.renderIds.indexOf(model.id);
				that.renderIds.splice(model.id, 1);
			});

			
		},
		scrollView: function() {
			var that = this;
			var scrolltop=	$(".iconView").scrollTop();
			var scrollheight=	$('.itemList').height();
			var windowheight=	$('.iconView ').height();
			var scrolloffset = 20;
			if(scrolltop>=(scrollheight-(windowheight+scrolloffset)))
			{
			    //подгружаем новые элементы
			   that.loadNewItems();
			   if(that.haveAnyMoreItems()) {
			   that.scrollView();
			   }
			}
			
		},
		haveAnyMoreItems: function() {
			var countOfItem = this.itemList.models.length;
			var counter =  0; 
			_.each(this.itemList.models,function(item) {
				if(_.contains(that.renderIds,item.id)) {
					counter++;
				}
			});
			//console.log(counter,countOfItem);
			return counter < countOfItem;
		},
		addItem: function() {
			var type = $( ".typeOfElement option:selected" ).val();

			var item = new IconItemModel({
				id: this.getNextId(),
				text: type,
				image: "images/" + type + ".png",
				checked: false
			});
			this.renderIds.push(item.id);
			this.itemList.add(item);
			this.renderItem(item);
		},
		getNextId: function() {
			var tmpId = 1;
			var ids = [];
			_.each(this.itemList.models,function(item){
				ids.push(item.id);
			});
			while(_.contains(ids,tmpId)){
				tmpId++;
			}
			return tmpId;
		},
		clickedOnItem: function(el) {
			var id = $(el.currentTarget).attr('data-id');

			if(this.isSelected(id)) {
				this.toggleCheckItems(id,false);
			} else {
				this.toggleCheckItems(id,true);
			}
		},

		toggleAllItems: function() {
			console.log('checked');
			var models = _.find(this.itemList.models,function (item){
				return item.get('checked') == true && _.contains(that.renderIds,item.id);
			});
			//console.log(models);
			var that = this;
			if(models != undefined) {
				_.each(this.itemList.models,function(item) {
					if(_.contains(that.renderIds,item.id)) {
						item.set('checked',false);
					}
				});
			} else {
				_.each(this.itemList.models,function(item){
					if(_.contains(that.renderIds,item.id)) {
						item.set('checked',true);
					}
				});
			}
		},
		toggleCheckItems: function(ids,toggle) {
			var models = [];
			var that = this;
			_.each([].concat(ids), function(id) {
				var state = _.find(that.itemList.models, function (item) {
					return item.id == id && _.contains(that.renderIds,item.id);

				});
				
				if(state != undefined) {
					state.set("checked",toggle);
				}
				
			});
			
		},

		isSelected: function(id) {

			var state = _.find(this.itemList.models, function (item) {
				return item.id == id;
			});
			
			if(state != undefined) {
				return state.attributes.checked;
			} else {
				return null
			}
		},
		loadNewItems: function() {
			var that = this;
			var counter = 1;
			//console.log(this.itemList.models.length);
			//console.log(that.renderIds.length);
			_.each(this.itemList.models, function (item) {
                if(that.initItemRenderCount >= counter && !_.contains(that.renderIds,item.id)){
 					that.renderItem(item);
                	that.renderIds.push(item.id);
                	counter++;
                }

            });
            return this;
		},
		render: function() {
			// set init width and height
			this.$el.find('.iconView').width(this.model.attributes.width).height(this.model.attributes.height);
			var that = this;
			// render items
			var counter = 1;
			_.each(this.itemList.models, function (item) {
                if(that.initItemRenderCount < counter){
 					return;
                }
                that.renderItem(item);
                that.renderIds.push(item.id);
                counter++;

            });

			return this;
		},
		renderItem: function(item) {

			var itemView = new ItemView({
			    model: item
			});

			this.$el.find('.' + this.itemListClass).append(itemView.render().el);
		}
	});
	exports.BrowserIconView = BrowserIconView;

})(window);