(function(exports) { 
	var IconItemModel = Backbone.Model.extend({
		idAttribute: "id",
		defaults: {
			text: "",
			image: "",
			checked: false
		}
	});
	exports.IconItemModel = IconItemModel;
})(window);