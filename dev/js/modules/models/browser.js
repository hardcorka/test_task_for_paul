(function(exports) { 
	var BrowserModel = Backbone.Model.extend({
		defaults: {
			width: 300,
			height: 300
		}
	});
	exports.BrowserModel = BrowserModel;
})(window);