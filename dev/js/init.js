$(document).ready(function() {
	// items
	var demoItems = [
		 {id:1, image:"images/photo.png", text:"photo", checked: false}
		,{id:2, image:"images/video.png", text:"video", checked: false}
		,{id:3, image:"images/text.png", text:"text", checked: true}
		,{id:4, image:"images/music.png", text:"music", checked: true}
		,{id:5, image:"images/photo.png", text:"photo", checked: false}
		,{id:6, image:"images/video.png", text:"video", checked: true}
		,{id:7, image:"images/music.png", text:"music", checked: true}
		,{id:8, image:"images/video.png", text:"video", checked: false}
		,{id:9, image:"images/photo.png", text:"photo", checked: false}
		,{id:10, image:"images/text.png", text:"text", checked: true}
		,{id:11, image:"images/video.png", text:"video", checked: true}
		,{id:12, image:"images/text.png", text:"text", checked: false}
		,{id:13, image:"images/video.png", text:"video", checked: false}
		,{id:14, image:"images/photo.png", text:"photo", checked: true}
		,{id:15, image:"images/music.png", text:"music", checked: true}
		,{id:16, image:"images/photo.png", text:"photo", checked: false}
		,{id:17, image:"images/text.png", text:"text", checked: true}
		,{id:18, image:"images/video.png", text:"video", checked: true}
		,{id:19, image:"images/video.png", text:"video", checked: false}
		,{id:20, image:"images/text.png", text:"text", checked: false}
		,{id:21, image:"images/video.png", text:"video", checked: false}
		,{id:22, image:"images/photo.png", text:"photo", checked: true}
		,{id:23, image:"images/music.png", text:"music", checked: true}
		,{id:24, image:"images/photo.png", text:"photo", checked: false}
		,{id:30, image:"images/text.png", text:"text", checked: true}
		,{id:31, image:"images/video.png", text:"video", checked: true}
		,{id:32, image:"images/text.png", text:"text", checked: false}
		,{id:33, image:"images/video.png", text:"video", checked: false}
		,{id:34, image:"images/photo.png", text:"photo", checked: true}
		,{id:35, image:"images/music.png", text:"music", checked: true}
		,{id:36, image:"images/photo.png", text:"photo", checked: false}
		,{id:37, image:"images/text.png", text:"text", checked: true}
		,{id:38, image:"images/video.png", text:"video", checked: true}
		,{id:40, image:"images/text.png", text:"text", checked: false}
	];

	
	window.itemlist = new Itemlist(demoItems);
	
	// console.log(itemlist);
	
	window.browserModel = new BrowserModel({
		width: 400, 
		height: 400
	});

	window.browserIconView = new BrowserIconView({
		el: $(".browserWrapper"),
		model: browserModel,
		itemlist: itemlist
	});

	$(".resizable")
      .wrap('<div/>')
        .css({'overflow':'hidden'})
          .parent()
            .css({'display':'inline-block',
                  'overflow':'hidden',
                  'height':function(){return $('.resizable',this).height();},
                  'width':  function(){return $('.resizable',this).width();},
                  // 'paddingBottom':'12px',
                  // 'paddingRight':'12px'
                  'padding':'10px'
                }).resizable()
                    .find('.resizable')
                      .css({overflow:'auto',
                            width:'100%',
                            height:'100%'});


});

